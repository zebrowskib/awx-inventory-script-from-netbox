# Pull Netbox As Inventory
This python script was designed to be added into AWX as an inventory script.
Once it has been added, you can create a regular inventory and use this script
as the source. It will reach into Netbox utilizing the variables at the top
which includes the URL to netbox as well as a token from a user account to pull
all devices and VMs as well as their interfaces and provide that information
for AWX to utilize.
netbox-script-gen-inventory.py = Used as a custom script inside netbox to output an inventory file
pull-netbox-as-inventory.py    = Used in AWX as a way to query Netbox for an inventory
