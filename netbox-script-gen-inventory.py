#!/usr/bin/env python
from dcim.models import Device, Interface, Platform
from ipam.models import IPAddress, VLAN
from virtualization.models import VirtualMachine, VMInterface
from extras.scripts import *
import json             # To format and display the final output
import ipaddress        # To get the network id without having to do an API call to find it

class Generate_Inventory(Script):
  class Meta:
    name = "Generate Ansible inventory from Netbox"
    description = "This will generate a json file to be used as an inventory source for Ansible"
    commit_default = False

  def run(self, data, commit):

    # Define what you want to group devices by. Ensure they are variables found in the devices API and not somewhere else.
    group_by = ['sites','tenant','rack','model','device_type','device_role']


    # First build our device list IF it has a primary_ip set
    hostvars = {}
    for dev in Device.objects.all():
      if dev.primary_ip is not None:                                      # This can be enabled if/when you want to only include devices with a primary IP
        hostvars[dev.name] = {
          'device_type': dev.device_type.model,               # Required
          'device_role': dev.device_role.name,                # Required
          'site': dev.site.name,                              # Required
          'model': dev.device_type.model,                     # Required
          'tags': [],                                         # placeholder for later
          'config_context': dev.get_config_context(),
          'interfaces': {}                                    # placeholder for later
        }
        if dev.tags.count() > 0:
          for tag_info in dev.tags.values():
            hostvars[dev.name]['tags'].append({
              'name': tag_info['name'],
              'slug': tag_info['slug']
            })
        # Not every device has a primary_ip assigned but we will add it to ansible anyway regardless if the 'if dev.primary_ip...' line above is commented out
        if dev.primary_ip:
          hostvars[dev.name]['ansible_host'] = str(ipaddress.IPv4Interface(dev.primary_ip.address).ip)
        # These next fields are not required. But because they need sub data (IE: Name) we can't do the same thing as config_context above.
        if dev.rack:
          hostvars[dev.name]['rack'] = dev.rack.name
        if dev.tenant:
          hostvars[dev.name]['tenant'] = dev.tenant.name
        if dev.platform:
          hostvars[dev.name]['platform'] = dev.platform.name
          # If the device's platform uses the ios napalm driver, then we need to add ansible vars to be able to interact with the device.
          if next(item for item in Platform.objects.all() if item.name == dev.platform.name).napalm_driver == 'ios':
            hostvars[dev.name].update({
              "ansible_become": "yes",
              "ansible_become_method": "enable",
              "ansible_connection": "network_cli",
              "ansible_network_os": "ios"
            })

    # Now for each interface, if its in our device list, add all of its interfaces
    for interfaces in Interface.objects.all():
      if interfaces.device.display_name in hostvars:
        hostvars[interfaces.device.display_name]['interfaces'][interfaces.name] = {
          'description': interfaces.description,
          'mac_address': str(interfaces.mac_address),
          'mtu': interfaces.mtu,
          'lag': interfaces.lag,
          'tagged_vlans': [],                                       # placeholder for later
          'tags': []                                                # placeholder for later interfaces.tags.values('name')
        }
        if interfaces.tags.count() > 0:
          for tag_info in interfaces.tags.values():
            hostvars[interfaces.device.display_name]['interfaces'][interfaces.name]['tags'].append({
              'name': tag_info['name'],
              'slug': tag_info['slug']
            })
        # These next fields are not required. But because they need sub data (IE: Name) or can be null we can't do the same thing as above.
        if interfaces.mode:
          hostvars[interfaces.device.display_name]['interfaces'][interfaces.name]['mode'] = interfaces.mode
        if interfaces.untagged_vlan:
          hostvars[interfaces.device.display_name]['interfaces'][interfaces.name]['untagged_vlan'] = interfaces.untagged_vlan.vid
        if interfaces.tagged_vlans:
          for vid in interfaces.tagged_vlans.values('vid'):
            hostvars[interfaces.device.display_name]['interfaces'][interfaces.name]['tagged_vlans'].append(vid['vid'])

    # Finally assign each host's interface with its IP, if its set.
    for ip in IPAddress.objects.all():
      # If the try fails its because its a static IP and not associated to an interface
      try:
        if ip.interface.device.name in hostvars:
          # Not sure if all of these are needed, but better to do the math in python than ansible
          # Print just the IP, remove the mask
          hostvars[ip.interface.device.name]['interfaces'][ip.interface.name]['ip'] = str(ipaddress.IPv4Interface(ip.address).ip)
          # Now just the full doted decimal notation of the subnet mask
          hostvars[ip.interface.device.name]['interfaces'][ip.interface.name]['netmask'] = str(ipaddress.IPv4Interface(ip.address).with_netmask.split('/')[1])
          # Now just the full doted decimal notation of the hostmask
          hostvars[ip.interface.device.name]['interfaces'][ip.interface.name]['hostmask'] = str(ipaddress.IPv4Interface(ip.address).with_hostmask.split('/')[1])
          # Find the network ID based off the IP/Mask
          hostvars[ip.interface.device.name]['interfaces'][ip.interface.name]['netid'] = str(ipaddress.IPv4Interface(ip.address).network)
          # Now use the netid and mask to determine the broadcast address
          hostvars[ip.interface.device.name]['interfaces'][ip.interface.name]['broadcast'] = str(ipaddress.IPv4Network(ipaddress.IPv4Interface(ipaddress.IPv4Interface(ip.address).network)).broadcast_address)
      except:
        pass

    # Now do the same thing as above for physical hardware but with virtual machines
    for dev in VirtualMachine.objects.all():
      if dev.primary_ip is not None:        # This can be enabled if/when you want to only include devices with a primary IP
        hostvars[dev.name] = {
          'device_type': 'VM',                                      # Required
          'device_role': dev.role.name,                             # Required
          'site': dev.site.name,                                    # Required
          'cluster': dev.cluster.name,                              # Required
          'tags': [],                                               # placeholder for later
#          'config_context': dev.get_config_context(),
          'vcpus': dev.vcpus,
          'memory': dev.memory,
          'disk': dev.disk,
          'interfaces': {}                                          # placeholder for later
        }
        if dev.tags.count() > 0:
          for tag_info in dev.tags.values():
            hostvars[dev.name]['tags'].append({
              'name': tag_info['name'],
              'slug': tag_info['slug']
            })
        # Not every device has a primary_ip assigned but we will add it to ansible anyway for now. Eventually these should go away and re-enable the 2 lines above
        if dev.primary_ip4:
          hostvars[dev.name]['ansible_host'] = str(ipaddress.IPv4Interface(dev.primary_ip4.address).ip)
        if dev.tenant:
          hostvars[dev.name]['tenant'] = dev.tenant.name
        if dev.platform:
          hostvars[dev.name]['platform'] = dev.platform.name

    # Create a lookup table for the next step
    vlan_array = {}
    for vlan_data in VLAN.objects.all():
      vlan_array[vlan_data.vid] = vlan_data.name

    # Now for each virtual machine interface, if its in our device list, add all of its interfaces
    for interfaces in VMInterface.objects.all():
      if interfaces.virtual_machine.name in hostvars:
        hostvars[interfaces.virtual_machine.name]['interfaces'][interfaces.name] = {
          'description': interfaces.description,
          'mac_address': str(interfaces.mac_address),
          'mtu': interfaces.mtu,
          'untagged_vlan': interfaces.untagged_vlan.display_name,
        }
        if interfaces.tags.count() > 0:
          for tag_info in interfaces.tags.values():
            hostvars[interfaces.virtual_machine.name]['interfaces'][interfaces.name]['tags'].append({
              'name': tag_info['name'],
              'slug': tag_info['slug']
            })
        # These next fields are not required. But because they need sub data (IE: Name) or can be null we can't do the same thing as above.
        if interfaces.mode:
          hostvars[interfaces.virtual_machine.name]['interfaces'][interfaces.name]['mode'] = interfaces.mode
        if interfaces.untagged_vlan:
          hostvars[interfaces.virtual_machine.name]['interfaces'][interfaces.name]['untagged_vlan'] = interfaces.untagged_vlan.vid
          hostvars[interfaces.virtual_machine.name]['interfaces'][interfaces.name]['untagged_vlan_name'] = vlan_array.get(interfaces.untagged_vlan.vid)
        if interfaces.tagged_vlans:
          for vid in interfaces.tagged_vlans.values('vid'):
            hostvars[interfaces.device.display_name]['interfaces'][interfaces.name]['tagged_vlans'].append(vid['vid'])

    # Now to group everything based off the list above. REMEMBER, this is based off the host data, NOT interface data.
    groups = {}
    for host in hostvars:
      # Setting a variable so we don't have to loop again later. If the host gets added to a group, then don't add it to the ungrouped group.
      found = False
      for group in group_by:
        # Adding try just in case you want to group by something that isn't defined for that particular host. IE: rack, tenant or platform
        try:
          if hostvars[host][group]:
            found = True
            groups.setdefault(hostvars[host][group], {'hosts': []}).update()
            groups[hostvars[host][group]]['hosts'].append(host)
            # Now add the hostname to the hosts section for each group
        except:
          pass
      if found == False:
        groups.setdefault('ungrouped', {'hosts': []}).update()
        groups['ungrouped']['hosts'].append(hosts)

    # Some quick queries for debug purposes
    #print(json.dumps(hostvars['<HOSTNAME>'], indent=4))

    # Final step, create one dictionary with both the groups and hostvars data and display it to the screen
    final_host_list = {'_meta': {'hostvars': hostvars}}
    # Now combine them. We could just pick one and update it, but this way its non-destructive so we can still debug later if need be.
    overall_list = {}
    overall_list.update(groups)
    overall_list.update(final_host_list)
    #with open('bla.txt','w') as output_file:
    #  json.dump(overall_list, output_file)
    return(json.dumps(overall_list, indent=4, sort_keys=True))
